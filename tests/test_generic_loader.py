# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import os
from minty_pyramid.generic_loader import GenericEngine, new_response_callback
from pyramid.config import Configurator
from pyramid.router import Router
from unittest import mock


class TestPyramidGenricEngine:
    @mock.patch("minty_pyramid.generic_loader.statsd")
    def test_engine_class(self, mock_statsd):
        engine = GenericEngine()

        global_config = {}
        settings = {
            "minty_service.infrastructure.config_file": os.path.dirname(
                __file__
            )
            + "/data/test.conf"
        }
        engine.setup(global_config, **settings)

        assert isinstance(engine.config, Configurator)
        mock_statsd.Connection.set_defaults.assert_called_once_with(
            disabled=True
        )
        wsgi = engine.main()
        assert isinstance(wsgi, Router)


class TestCacheControl:
    def test_set_cache_control_header(self):
        response = mock.MagicMock()
        response.configure_mock(status=200, headers={})

        event = mock.MagicMock()
        event.configure_mock(response=response)

        new_response_callback(event)
        assert event.response.headers["cache-control"] == "no-store"

    def test_set_cache_control_header_no_override(self):
        response = mock.MagicMock()
        response.configure_mock(
            status=200, headers={"cache-control": "no-cache"}
        )

        event = mock.MagicMock()
        event.configure_mock(response=response)

        new_response_callback(event)
        assert event.response.headers["cache-control"] == "no-cache"
        assert event.response.headers["request-id"] == str(
            event.request.request_id
        )
