# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import os
import pytest
from minty.infrastructure import InfrastructureFactory
from minty.repository import RepositoryBase
from minty_pyramid.generic_loader import (
    RequestDataLogger,
    RequestLoadGauge,
    RequestTimer,
    get_logging_info,
    new_response_callback,
)
from minty_pyramid.loader import (
    Engine,
    _build_cqrs_setup,
    _build_http_session_manager,
)
from minty_pyramid.session_manager import SessionRetriever
from pyramid.config import Configurator
from pyramid.router import Router
from typing import Dict, Type
from unittest import mock
from uuid import uuid4


class MockConfig:
    def __init__(self):
        self.request_methods = {}

    def add_request_method(self, method, name, property=False, reify=False):
        self.request_methods[name] = method


class MockQueryInstance:
    def __init__(self, correlation_id, domain, context, user_uuid, user_info):
        self.correlation_id = correlation_id
        self.domain = domain
        self.context = context
        self.user_uuid = user_uuid
        self.user_info = user_info

        self.query_instance = self


class MockCommandInstance:
    def __init__(self, correlation_id, domain, context, user_uuid, user_info):
        self.correlation_id = correlation_id
        self.domain = domain
        self.context = context
        self.user_uuid = user_uuid
        self.user_info = user_info

        self.command_instance = self


class MockCQRS:
    def __init__(self):
        pass

    def get_query_instance(
        self, correlation_id, domain, context, user_uuid, user_info=None
    ):
        return MockQueryInstance(
            correlation_id, domain, context, user_uuid, user_info
        )

    def get_command_instance(
        self, correlation_id, domain, context, user_uuid, user_info=None
    ):
        return MockCommandInstance(
            correlation_id, domain, context, user_uuid, user_info
        )


class MockHandler:
    def __init__(self, status):
        self.status = status

    def __call__(self, request):
        return MockResponse(self.status)


class MockResponse:
    def __init__(self, status, headers={}):
        self.status_int = status
        self.headers = headers


class MockRequest:
    @property
    def path(self):
        return "/path"

    @property
    def host(self):
        return "example.com"

    @property
    def request_id(self):
        return uuid4()

    def set_matched_route(self, name):
        class Name:
            def __init__(self, name):
                self.name = name

        self.matched_route = Name(name)


class MockNewResponseEvent:
    def __init__(self, response):
        self.response = response
        self.request = mock.MagicMock()

        self.request.request_id = uuid4()


class MockDomain:
    REQUIRED_REPOSITORIES: Dict[str, Type[RepositoryBase]] = {}


class TestCacheControl:
    def test_set_cache_control_header(self):
        response = MockResponse(status=200)
        event = MockNewResponseEvent(response)
        new_response_callback(event)
        assert event.response.headers["cache-control"] == "no-store"

    def test_set_cache_control_header_no_override(self):
        response = MockResponse(
            status=200, headers={"cache-control": "no-cache"}
        )
        event = MockNewResponseEvent(response)
        new_response_callback(event)
        assert event.response.headers["cache-control"] == "no-cache"
        assert event.response.headers["request-id"] == str(
            event.request.request_id
        )


class TestPyramidLoader:
    def test_setup_cqrs_request(self):
        mock_config = MockConfig()
        mock_cqrs = MockCQRS()
        user_uuid = uuid4()

        setup_callable = _build_cqrs_setup(mock_cqrs)
        setup_callable(mock_config)

        assert "get_query_instance" in mock_config.request_methods

        mock_method = mock_config.request_methods["get_query_instance"]
        query_instance = mock_method(MockRequest(), "SomeDomain", user_uuid)

        assert query_instance.domain == "SomeDomain"
        assert query_instance.context == "example.com"

        assert "get_command_instance" in mock_config.request_methods

        mock_method = mock_config.request_methods["get_command_instance"]
        command_instance = mock_method(MockRequest(), "SomeDomain", user_uuid)

        assert command_instance.domain == "SomeDomain"
        assert command_instance.context == "example.com"

    @mock.patch("minty_pyramid.loader.Configurator.add_request_method")
    def test_setup_request_with_infrastructure_factory(
        self, mock_add_request_method
    ):
        infra_factory_mock = mock.MagicMock()
        infra_factory_mock.return_value = "infrastructure_factory"
        engine = Engine([MockDomain])
        global_config = {}
        settings = {
            "minty_service.infrastructure.config_file": os.path.dirname(
                __file__
            )
            + "/data/test.conf"
        }
        engine.setup(global_config, **settings)

        name, args, kwargs = mock_add_request_method.mock_calls[3]
        request_method = args[0](infra_factory_mock)

        assert isinstance(request_method, InfrastructureFactory)
        assert kwargs == {
            "property": True,
            "reify": True,
            "name": "infrastructure_factory",
        }

    @mock.patch.object(SessionRetriever, "retrieve")
    def test_retrieve_session_method(self, mock_SessionRetriever):
        m_session = mock_SessionRetriever({})

        setup_session_request = _build_http_session_manager(m_session)
        config = MockConfig()
        setup_session_request(config)

        m_session.retrieve.return_value = "session goes here"

        mock_request = mock.MagicMock()
        mock_request.cookies = {"minty_cookie": 100}
        mock_request.session_id = 100

        callback = config.request_methods["retrieve_session"]
        rv = callback(mock_request)
        m_session.retrieve.assert_called_once_with(100)

        mock_request = mock.MagicMock()
        mock_request.cookies = {"minty_cookie": 123}
        callback = config.request_methods["session_id"]
        rv = callback(mock_request)

        assert rv == 123
        mock_request.cookies = {}
        callback = config.request_methods["session_id"]
        rv = callback(mock_request)
        assert rv == "<session cookie not found>"


class TestRequestDataLogger:
    def test_get_logging_info(self):
        request = mock.MagicMock()
        request.host = "testhost"
        request.session_id = "test_session_id"
        request.request_id = "aaaaaa"
        request.configuration = {"instance_hostname": "configured_hostname"}
        request.path_info = "action_path"

        assert get_logging_info(request) == {
            "hostname": "testhost",
            "session_id": "test_session_id",
            "request_id": "aaaaaa",
            "instance_hostname": "configured_hostname",
            "action_path": "action_path",
        }

    def test_get_logging_info_unconfigured(self):
        request = mock.MagicMock()
        request.host = "testhost"
        del request.session_id
        request.request_id = "aaaaaa"
        request.configuration = {"instance_hostname": "configured_hostname"}
        request.path_info = "action_path"

        assert get_logging_info(request) == {
            "hostname": "testhost",
            "session_id": "<sessions not configured>",
            "request_id": "aaaaaa",
            "instance_hostname": "configured_hostname",
            "action_path": "action_path",
        }

    def test_data_logger(self):
        handler = mock.MagicMock()
        handler.return_value = "handled"

        registry = mock.MagicMock()
        request = mock.MagicMock()
        request.host = "testhost"
        request.session_id = "test_session_id"

        rdl = RequestDataLogger(handler, registry)

        rv = rdl(request)
        assert rv == "handled"

    def test_data_logger_no_session(self):
        handler = mock.MagicMock()
        handler.return_value = "handled"

        registry = mock.MagicMock()

        rdl = RequestDataLogger(handler, registry)

        request = mock.MagicMock()
        request.host = "foobar"
        request.request_id = 42
        request.path_info = "current_path"

        rv = rdl(request)
        assert rv == "handled"


class TestPyramidEngine:
    def test_engine_class(self):
        engine = Engine([MockDomain])

        assert engine.domains == [MockDomain]

        with pytest.raises(ValueError):
            engine.main()

        global_config = {}
        settings = {
            "minty_service.infrastructure.config_file": os.path.dirname(
                __file__
            )
            + "/data/test.conf"
        }
        engine.setup(global_config, **settings)

        assert isinstance(engine.config, Configurator)

        wsgi = engine.main()
        assert isinstance(wsgi, Router)

    @mock.patch("minty_pyramid.loader.Configurator.add_tween")
    def test_engine_class_add_tween(self, mock_add_tween):
        engine = Engine([MockDomain])
        settings = {
            "minty_service.infrastructure.config_file": os.path.dirname(
                __file__
            )
            + "/data/test.conf"
        }
        global_config = {}
        engine.setup(global_config, **settings)
        mock_add_tween.assert_has_calls(
            [
                mock.call("minty_pyramid.generic_loader.RequestLoadGauge"),
                mock.call("minty_pyramid.generic_loader.RequestTimer"),
                mock.call("minty_pyramid.generic_loader.RequestDataLogger"),
            ]
        )

    @mock.patch("minty_pyramid.loader.session_manager_factory")
    def test_engine_class_with_session_manager(
        self, mock_session_manager_factory
    ):

        engine = Engine([MockDomain])
        global_config = {}
        settings = {
            "minty_service.infrastructure.config_file": os.path.dirname(
                __file__
            )
            + "/data/test.conf",
            "session_manager": True,
        }
        engine.setup(global_config, **settings)
        mock_session_manager_factory.assert_called_once()
        assert isinstance(engine.config, Configurator)
        wsgi = engine.main()
        assert isinstance(wsgi, Router)


class TestRequestTimer:
    @mock.patch("minty.Base.statsd")
    def test_request_timer_OK(self, mock_statsd):
        mock_statsd.get_timer.return_value = mock_statsd
        mock_statsd.get_counter.return_value = mock_statsd

        mock_handler = MockHandler(status="200")

        mock_request = MockRequest()
        mock_request.set_matched_route("hello_request")

        handle_request = RequestTimer(handler=mock_handler, registry={})

        handle_request(request=mock_request)

        mock_statsd.get_timer.assert_called_with("request_duration")
        mock_statsd.get_timer().time.assert_called_with("path")

    @mock.patch("minty.Base.statsd")
    def test_request_timer_not_OK(self, mock_statsd):
        mock_statsd.get_timer.return_value = mock_statsd
        mock_statsd.get_counter.return_value = mock_statsd

        mock_handler = MockHandler(status="404")
        mock_request = MockRequest()

        handle_request = RequestTimer(handler=mock_handler, registry={})
        handle_request(request=mock_request)

        mock_statsd.get_counter.assert_called_with("number_of_requests")
        mock_statsd.get_counter().increment.assert_called_with("404", delta=1)


class TestRequestLoadGauge:
    @mock.patch("minty.Base.statsd")
    def test_request_load_gauge_OK(self, mock_statsd):
        mock_statsd.get_gauge.return_value = mock_statsd

        mock_handler = MockHandler(status="200")
        mock_request = MockRequest()

        mock_request.set_matched_route("hello_request")

        handle_request = RequestLoadGauge(handler=mock_handler, registry={})

        handle_request(request=mock_request)

        mock_statsd.get_gauge.assert_called_with("active_threads")
        mock_statsd.get_gauge().increment.assert_called_with()
        mock_statsd.get_gauge().decrement.assert_called_with()

    @mock.patch("minty.Base.statsd")
    def test_request_load_gauge_not_OK(self, mock_statsd):
        mock_statsd.get_gauge.return_value = mock_statsd

        mock_handler = MockHandler(status="404")
        mock_request = MockRequest()

        handle_request = RequestLoadGauge(handler=mock_handler, registry={})
        handle_request(request=mock_request)

        mock_statsd.get_gauge.assert_called_with("active_threads")
        mock_statsd.get_gauge().increment.assert_called_with()
        mock_statsd.get_gauge().decrement.assert_called_with()
